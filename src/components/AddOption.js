import React from 'react';


export default class AddOption extends React.Component{

    state={
        error:undefined
    };
    

    handleDdoption=(e)=>{
     e.preventDefault();
     
    
     const op=e.target.elements.aoption.value.trim();

       const error= this.props.handleDdoption(op);
       this.setState(()=>({error}));
       if(!error){
           e.target.elements.aoption.value='';
       }
     
 };


    render(){
        return (
            <div>
                {this.state.error&&<p className="add-option-error">{this.state.error}</p>}
                <form onSubmit={this.handleDdoption} className="add-option">

<input  className="add-option__input" type="Text" name="aoption"></input>
<button className="button">AddOption</button>

                </form>
            </div>
        );
    }
}