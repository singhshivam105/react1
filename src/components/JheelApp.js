

import React from 'react';
import AddOption from './AddOption';

import Header from './Header';
import Action from './Action';
import Options from './Options';
import OptionModal from './OptionModal';




export default class JheelApp extends React.Component{

    state=  {
        options:[],
        selectedOption: undefined
        //options:this.props.options
    };



    ///handle delete logic
    handleDeleteOptions=()=>{
        this.setState(()=>({options:[]}));
    }

    handleClearSelectedItem=()=>{
        this.setState(()=>({
            selectedOption:undefined
        }));
    }

    handleDeleteOption=(opstoremove)=>{
        this.setState((abcd) => ({
            options: abcd.options.filter((option) => {
            return opstoremove !== option; 
        })}));
    }

//react lifecyxle

componentDidMount(){
try{
    const json=localStorage.getItem('options');
    const options=JSON.parse(json);
    if(options){
    this.setState(()=>({options}))
    }
}
catch(e){

}
console.log('fetching data');
}
componentDidUpdate(prvprops,prevState){
    if(prevState.options.length !== this.state.options.length){
        const jsom=JSON.stringify(this.state.options);
        localStorage.setItem('options', jsom );
    }
console.log('saving data');
}


componentWillMount(){
console.log('component will unmount');
}

    handlePick=()=>{
        const randomN=Math.floor(Math.random()*this.state.options.length);
        const optionsd=this.state.options[randomN];
       
        this.setState(()=>({
           selectedOption:optionsd 
        }))
    }
        
    handleDdoption=(op)=>{

        if(!op){
            return 'Enter a vailid value';
        }else if(this.state.options.indexOf(op)>-1){
            return 'This item is duplicate value';
        }

        this.setState((pstate)=>({options:pstate.options.concat([op])}));
    
        
    }


    render(){
        const title='JheelAPP';
        const subtitle='Go ahead it will work!! as expected';
      
        
        return (

            <div>
   
            <Header title={title} subtitle={subtitle}/>
            <div className="container">
            <Action hasOption={this.state.options.length>0} handlePick={this.handlePick} />
            <div className="widget">
            <Options options={this.state.options} handleDeleteOptions={this.handleDeleteOptions} handleDeleteOption={this.handleDeleteOption}/>
            <AddOption handleDdoption={this.handleDdoption}/>
            </div>
            <OptionModal selectedOption={this.state.selectedOption}  handleClearSelectedItem={this.handleClearSelectedItem}/>
                </div>
           
            </div>   
        );
    }
}
