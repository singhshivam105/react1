import React from 'react';
import OneOption from './OneOption';


const Options=(props)=>(

<div>
    <div className="widget-header">
        <h3 className="widget-header__title">Your Option</h3>
    <button onClick={props.handleDeleteOptions} className="button button-link">RemoveAll</button>
    </div>
    {props.options.length===0 && <p className="widget__message">Please add an option</p>}
        {props.options.map((option,index)=>(
    <OneOption
     key={option} 
     optionText={option}
     count={index+1}
     handleDeleteOption={props.handleDeleteOption}
     />))}
    
</div>);
    export default Options;