
class JheelApp extends React.Component{

    constructor(props){
        super(props)

        this.state=  {
            options:[]
            //options:this.props.options
        };

         
            this.handleDeleteOptions=this.handleDeleteOptions.bind(this);
            this.handlePick=this.handlePick.bind(this);
            this.handleDdoption=this.handleDdoption.bind(this);
            this.handleDeleteOption=this.handleDeleteOption.bind(this);
        
    }

    ///handle delete logic
    handleDeleteOptions(){
        this.setState(()=>({options:[]}));
    }
    handleDeleteOption(opstoremove){
        this.setState((abcd) => ({
            options: abcd.options.filter((option) => {
            return opstoremove !== option; 
        })}));
    }

//react lifecyxle

componentDidMount(){
try{
    const json=localStorage.getItem('options');
    const options=JSON.parse(json);
    if(options){
    this.setState(()=>({options}))
    }
}
catch(e){

}
console.log('fetching data');
}
componentDidUpdate(prvprops,prevState){
    if(prevState.options.length !== this.state.options.length){
        const jsom=JSON.stringify(this.state.options);
        localStorage.setItem('options', jsom );
    }
console.log('saving data');
}


componentWillMount(){
console.log('component will unmount');
}

    handlePick(){
        const randomN=Math.floor(Math.random()*this.state.options.length);
        const optionsd=this.state.options[randomN];
        alert(optionsd);
    }
        
    handleDdoption(op){

        if(!op){
            return 'Enter a vailid value';
        }else if(this.state.options.indexOf(op)>-1){
            return 'This item is duplicate value';
        }

        this.setState((pstate)=>({options:pstate.options.concat([op])}));
    
        
    }


    render(){
        const title='JheelAPP';
        const subtitle='Go ahead it will work!! as expected';
      
        
        return (

            <div>
   
            <Header title={title} subtitle={subtitle}/>
            <Action hasOption={this.state.options.length>0} handlePick={this.handlePick} />
            <Options options={this.state.options} handleDeleteOptions={this.handleDeleteOptions} handleDeleteOption={this.handleDeleteOption}/>
            <AddOption handleDdoption={this.handleDdoption}/>
            </div>   
        );
    }
}


const Header=(props)=>{
    return(
        <div>
        <h1>{props.title}</h1>
<h2>{props.subtitle }</h2>
</div> 
    );
}
/*
class Header extends React.Component{
    render(){
        console.log(this.props);
        return (

<div>
        <h1>{this.props.title}</h1>
<h2>{this.props.subtitle }</h2>
</div>  );

    }
}
*/

const Action=(props)=> { return(
    <div>
<button onClick={props.handlePick}
disabled={!props.hasOption}>what should i do!</button>
    </div>
    )
}
/*
class Action extends React.Component{

     render(){
         return(
        <div>
<button onClick={this.props.handlePick}
disabled={!this.props.hasOption}>what should i do!</button>
        </div>
        )};
}
*/

const Options=(props)=>{
return(<div>
    <button onClick={props.handleDeleteOptions}>RemoveAll</button>
    {props.options.length===0 && <p>pleas add an option</p>}
        {props.options.map((option)=>(
    <OneOption
     key={option} 
     optionText={option}
     handleDeleteOption={props.handleDeleteOption}
     />))}
    
</div>)

}
/*
class Options extends React.Component{

 
    render(){

       
        return(
<div>
    <button onClick={this.props.handleDeleteOptions}>RemoveAll</button>
    <h1>{this.props.options.length}</h1>
    {this.props.options.map(num=>(<OneOption key={num} optionText={num} />))}
    
</div>
        );
    }
}
*/
const OneOption=(props)=>{
    return(    <div>
        Options are:{props.optionText}
        <button onClick={(e)=>{
            props.handleDeleteOption(props.optionText);
        }}>remove</button>
        </div>)
}
/*
class OneOption extends React.Component{
    render(){
        return(
            <div>
Options are:{this.props.optionText}
</div>
        );
    } 
}
*/



class AddOption extends React.Component{

    constructor(props){
        super(props);
        this.handleDdoption=this.handleDdoption.bind(this);
        this.state={
            error:undefined
        };
    }

    handleDdoption(e){
     e.preventDefault();
    
     const op=e.target.elements.aoption.value.trim();

       const error= this.props.handleDdoption(op);
       this.setState(()=>({error}));
       if(!error){
           e.target.elements.aoption.value='';
       }
     
 }


    render(){
        return (
            <div>
                {this.state.error&&<p>{this.state.error}</p>}
                <form onSubmit={this.handleDdoption}>

<input type="Text" name="aoption"></input>
<button>AddOption</button>

                </form>
            </div>
        );
    }
}

  /*
const User=(props)=>{
    return (
        <div>
            <p>Name: {props.name}</p>
            <p>Age: {props.age} </p>
        </div>
    );
}
*/
/*for default value stateleas component
JheelApp.defaultProps={
    options:[] 
}
*/
ReactDOM.render(<JheelApp options={['ram','shyam']} />, document.getElementById('app'));