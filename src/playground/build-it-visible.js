class BuildVisible extends React.Component{

constructor(props){

    super(props);
    this.ButtonChange=this.ButtonChange.bind(this);
    this.state={
        visibility: false
    }
}


ButtonChange(){

    this.setState((prevS)=>{
        return {
            visibility: !prevS.visibility
        }
    });

}

    render(){
        return (

            <div>
                <h1>Toggle Test</h1>
        <button onClick={this.ButtonChange}>{this.state.visibility?'hideDetails':'showDetails'}</button>
            
            {this.state.visibility&&(
                <div>
                <p>you are looking at details</p>
                </div>
            )}
            </div>
        ); 
    }
}



ReactDOM.render(<BuildVisible /> ,document.getElementById('app'));














/*
let visibility=false;

const ButtonChange=()=>{
    visibility=!visibility;
    render();
}

const render=()=>{


    const toggleinfo=(
        <div>
        <h1>Toggle Test</h1>
    <button onClick={ButtonChange}>{visibility?'hideDetails':'showDetails'}</button>
    {
        visibility&&(
            <div>
            <p>you are looking at details</p>
            </div>
        )
    }
    </div>
    );

    
ReactDOM.render(toggleinfo,document.getElementById('app'));



};

render();
*/