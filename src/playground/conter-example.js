class Counter extends React.Component{

    constructor(props){
        super(props);
        this.handleAddOne=this.handleAddOne.bind(this);
        this.handleMinusOne=this.handleMinusOne.bind(this);
        this.handleReset=this.handleReset.bind(this);
        this.state={
            count:props.count
        };
    }

    handleAddOne(){
        this.setState((pstate)=>{
            return {
                count:pstate.count+1
            } ;
        });
    }

    handleMinusOne(){
        

        this.setState((mstate)=>{
            return {
                count:mstate.count-1
            };
        });

    }

    handleReset(){
        this.setState((abc)=>{
            return{
                count:0
            }
        })
    }

    render(){
        return (
            <div>
                <h1>Count:{this.state.count}</h1>
                <button onClick={this.handleAddOne}>+1</button>
                <button onClick={this.handleMinusOne}>-1</button>
                <button onClick={this.handleReset}>reset</button>
            </div>

        );
    }
}

Counter.defaultProps ={
count:0
}; 

ReactDOM.render(<Counter />, document.getElementById('app'));


/*
let count=0;
const addone=()=>{
    count++;
    renderVariable();
}
const mone=()=>{
    count--;
    renderVariable();
}
const resetone=()=>{
    count=0;
    renderVariable();
}
const approot=document.getElementById('app');
const renderVariable=()=>{
const template3=(
    <div>
   <h1>Count is: {count} </h1>
<button onClick={addone}>Button1</button>
<button onClick={mone}>Button-1</button>
<button onClick={resetone}>reset</button>
   </div>
);
ReactDOM.render(template3,approot);
}

renderVariable();
*/