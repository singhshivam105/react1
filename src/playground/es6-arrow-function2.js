///////////argument object ----no longer boud with arow functions
//////////If we keep like below will get error undefined name in this.name
/*
const user={
    name:'shivam singh',
    age:24,
    citylived:['Mumbai','Pune','Gurgaon','Canada','Chennai','Dublin'],
    locationDetails:function(){
    
        this.citylived.forEach(function(city){
            console.log(this.name+'lived in'+city);
        })
    }

}

user.locationDetails();
*/
///////////////////We can fix error like below////////////////
/*
const user={
    name:'shivam singh',
    age:24,
    citylived:['Mumbai','Pune','Gurgaon','Canada','Chennai','Dublin'],
    locationDetails:function(){
    const that=this;
    that.citylived.forEach(function(city){
            console.log(that.name+'lived in'+city);
        })
    }

}

user.locationDetails();
*/
//////////this is not bound in arrow function so if we use arrow function there is no need to write that=this
////////////Below will work /////////////////////////
/*
const user={
    name:'shivam singh',
    age:24,
    citylived:['Mumbai','Pune','Gurgaon','Canada','Chennai','Dublin'],
    locationDetails:function(){
   
    this.citylived.forEach((city)=>{
            console.log(this.name+'lived in'+city);
    });
    }

};
user.locationDetails();

*/


////////////Also we can remove the function word and can make a function directly

/*
const user={
    name:'shivam singh',
    age:24,
    citylived:['Mumbai','Pune','Gurgaon','Canada','Chennai','Dublin','Goa'],
    locationDetails(){
    
    this.citylived.forEach((city)=>{
            console.log(this.name+'lived in'+city);
    });
    }

};
user.locationDetails();
*/


////////////////////we can use map also///////////////////

const user={
    name:'shivam singh',
    age:24,
    citylived:['Mumbai','Pune','Gurgaon','Canada','Chennai','Dublin','Goa'],
    locationDetails(){
   let ab=this.citylived.map(function(city){
       return city;
   });
   return ab;
    }
};
console.log(user.locationDetails());


////////////an example that returns an array //////////////
const num={
    number:[3,4,6,7],
    mulb:3,
    rmap(){
      return  this.number.map((numberb)=>numberb*this.mulb);
    }
}
console.log(num.rmap());
























