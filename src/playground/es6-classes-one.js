 class Person{
constructor(name='Anonymous',age=0){
    this.name=name;
    this.age=age;
 }

 getGreetings()
{
    return `my name is  ${this.name}`;
}
 getDetails(){
 return `name is ${this.name}+age details is ${this.age}`;
 }
 }

class Student extends Person{

    constructor(name,age,major='none'){
        super(name,age);
        this.major=major;
    }
    hasMajor(){
        return !!this.major;
    }

    getDetails(){
        let des=super.getDetails();
        if(this.hasMajor()){
            des+=`major is ${this.major}`;
        }
        return des;
        }
}


class Traveler extends Person{

    constructor(name,age,homeLocation){
        super(name,age);
        this.homeLocation=homeLocation;
    }

    
    getGreetings(){
        let greeting=super.getGreetings();
        if(this.homeLocation){
            greeting+=` visting from ${this.homeLocation}`;
        }
        return greeting;
    }

}

 const student=new Student('ram',25,'CSE');
 console.log(student.getDetails());
 console.log(student.hasMajor());
 console.log(student);



const travel1=new Traveler('tarun',25,'Maghi')
console.log(travel1.getGreetings());

const travel2=new Traveler('pintoo',undefined,'asansol')
console.log(travel2.getGreetings());

const travel3=new Traveler(undefined,undefined,'kunda')
console.log(travel3.getGreetings());

