
//////in var we can redefine
var name='shivam';
var name='lalu';
console.log(name);

///in let we cann not redefine if we take let name1='lalu' then will get error
let name1='shivam';
 name1='lalu';

console.log(name1);

////In const neither we can redefine nor reassign
const name2='cshivam';
 // name2='clalu';
console.log(name1);

function checkDa(){
    var name3='Rahul';
    return name3;
}

console.log(checkDa());
///name 3 is not accessible outside the method hence we get error and no output
///console.log(name3);

//////////////////Check in block//////////////////
////////Inside the block if we declare any local variable the if it is var then only we can access outside the block in case of let and
///////const we cann not
const name4='Shivam Singh';

if(name4){
    var name5=name4+'correct';

}

console.log(name5);

////////////correct approcach to do if we want to do similar to above using let and const do like below

const name6="shivam";
let name7;
if(name6){
    name7=name6+'checking let';
}

console.log(name7);
















